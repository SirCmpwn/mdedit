﻿using System;
using Xwt;
using System.IO;
using System.Linq;
using System.Reflection;

namespace mdedit
{
    public class MDWindow : Window
    {
		public Notebook TabView { get;	set; }
        public WebView WebView { get; set; }
        public MenuItem FileMenu { get; set; }
            public MenuItem OpenMenuItem { get; set; }
            public MenuItem SaveMenuItem { get; set; }
            public MenuItem ExitMenuItem { get; set; }
        public MenuItem OptionsMenu { get; set; }
            public RadioButtonMenuItem BoringModeMenuItem { get; set; }
            public RadioButtonMenuItem VimModeMenuItem { get; set; }
            public RadioButtonMenuItem EmacsModeMenuItem { get; set; }
            public RadioButtonMenuItem SublimeModeMenuItem { get; set; }
        public MDService Service { get; set; }

        public string FileName { get; set; }

        public MDWindow(MDService service, string[] args)
        {
            Service = service;
			Service.WebSocketConnected += Socket_Connected;
            Service.DocumentAction += Service_DocumentAction;

            this.Width = 1200;
            this.Height = 576;
            this.Title = "mdedit - New file";

			if (args.Length > 0 && args[0] == "--remote")
			{
				args = args.Skip(1).ToArray();
				System.Diagnostics.Process.Start(string.Format("http://127.0.0.1:{0}/index.html", Service.Port));
			}
			else
			{
				WebView = new WebView(string.Format("http://127.0.0.1:{0}/index.html", Service.Port));
				TabView = new Notebook();
				TabView.Add(WebView, "[new file]");
				Content = TabView;
			}
			// TODO: Load files from args

            MainMenu = new Menu();
            // File
            FileMenu = new MenuItem("File") { SubMenu = new Menu() };
            MainMenu.Items.Add(FileMenu);
                // File > Open
                OpenMenuItem = new MenuItem("Open");
                OpenMenuItem.Clicked += OpenMenuItem_Clicked;
                FileMenu.SubMenu.Items.Add(OpenMenuItem);
                // File > Save
                SaveMenuItem = new MenuItem("Save");
                SaveMenuItem.Clicked += SaveMenuItem_Clicked;
                FileMenu.SubMenu.Items.Add(SaveMenuItem);
                // ---
                FileMenu.SubMenu.Items.Add(new SeparatorMenuItem());
                // File > Exit
                ExitMenuItem = new MenuItem("Exit");
                ExitMenuItem.Clicked += (sender, e) => this.Close();
                FileMenu.SubMenu.Items.Add(ExitMenuItem);
            // Options
            OptionsMenu = new MenuItem("Options") { SubMenu = new Menu() };
            MainMenu.Items.Add(OptionsMenu);
                // Options > ... Mode
                BoringModeMenuItem = new RadioButtonMenuItem("Boring mode");
                BoringModeMenuItem.Clicked += BoringModeMenuItem_Clicked;
                OptionsMenu.SubMenu.Items.Add(BoringModeMenuItem);
                VimModeMenuItem = new RadioButtonMenuItem("Vim mode");
                VimModeMenuItem.Clicked += VimModeMenuItem_Clicked;
                OptionsMenu.SubMenu.Items.Add(VimModeMenuItem);
                EmacsModeMenuItem = new RadioButtonMenuItem("Emacs mode");
                EmacsModeMenuItem.Clicked += EmacsModeMenuItem_Clicked;
                OptionsMenu.SubMenu.Items.Add(EmacsModeMenuItem);
                SublimeModeMenuItem = new RadioButtonMenuItem("Sublime mode");
                SublimeModeMenuItem.Clicked += SublimeModeMenuItem_Clicked;
                OptionsMenu.SubMenu.Items.Add(SublimeModeMenuItem);
                BoringModeMenuItem.Group = new RadioButtonMenuItemGroup();
                VimModeMenuItem.Group = EmacsModeMenuItem.Group = SublimeModeMenuItem.Group = BoringModeMenuItem.Group;
                BoringModeMenuItem.Checked = true;
        }

        void SublimeModeMenuItem_Clicked(object sender, EventArgs e)
        {
            Service.SetKeyMap("sublime");
        }

        void EmacsModeMenuItem_Clicked(object sender, EventArgs e)
        {
            Service.SetKeyMap("emacs");
        }

        void VimModeMenuItem_Clicked(object sender, EventArgs e)
        {
            Service.SetKeyMap("vim");
        }

        void BoringModeMenuItem_Clicked(object sender, EventArgs e)
        {
            Service.SetKeyMap("default");
        }

        void Service_DocumentAction(object sender, DocumentActionEventArgs e)
        {
            switch (e.Action)
            {
                case "save":
                    SaveMenuItem_Clicked(sender, e);
                    break;
            }
        }

        void Socket_Connected(object sender, EventArgs e)
        {
			// Load up the README as the default document
			using (var reader = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("mdedit.README.md")))
			{
				var text = reader.ReadToEnd();
				Service.SetText(text);
			}
        }

        void SaveMenuItem_Clicked(object sender, EventArgs e)
        {
            if (FileName == null)
            {
                var dialog = new SaveFileDialog();
                if (!dialog.Run())
                    return;
                FileName = dialog.FileName;
                Title = Path.GetFileName(FileName);
            }
            Service.GetText(text =>  File.WriteAllText(FileName, text));
        }

        void OpenMenuItem_Clicked(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filters.Add(new FileDialogFilter("Markdown files", "*.md"));
            if (dialog.Run())
            {
                var text = File.ReadAllText(dialog.FileName);
                Service.SetText(text);
                FileName = dialog.FileName;
                Title = Path.GetFileName(FileName);
            }
        }
    }
}