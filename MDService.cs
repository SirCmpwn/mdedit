﻿using System;
using Griffin.Networking.Messaging;
using Griffin.Networking.Protocol.Http;
using System.Net;
using Griffin.Networking.Protocol.Http.Protocol;
using System.Reflection;
using System.IO;
using System.Text;
using SuperWebSocket;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace mdedit
{
    public class MDService
    {
        public int Port { get; set; }
        public WebSocketSession SocketSession { get; set; }
        public event EventHandler WebSocketConnected;
        public event EventHandler<DocumentActionEventArgs> DocumentAction;

        private Dictionary<int, Action<string>> PendingCallbacks { get; set; }
        private int Transaction { get; set; }

        public MDService()
        {
            PendingCallbacks = new Dictionary<int, Action<string>>();
            Transaction = 0;

            var server = new MessagingServer(new HttpServiceWrappper.ServiceFactory(RequestHandler),
                new MessagingServerConfiguration(new HttpMessageFactory()));
            server.Start(new IPEndPoint(IPAddress.Loopback, 0));
            Port = server.LocalPort;

            var socket = new WebSocketServer();
            socket.Setup(53497);
            socket.Start();
            socket.NewSessionConnected += Socket_NewSessionConnected;
            socket.NewMessageReceived += Socket_NewMessageReceived;
        }

        void Socket_NewMessageReceived(WebSocketSession session, string value)
        {
            var data = JToken.Parse(value);
            #if DEBUG
            Console.WriteLine(value);
            #endif
            switch (data["command"].Value<string>())
            {
                case "getText":
                    var callback = PendingCallbacks[data["transaction"].Value<int>()];
                    PendingCallbacks.Remove(data["transaction"].Value<int>());
                    callback(data["text"].Value<string>());
                    break;
                case "action":
                    if (DocumentAction != null)
                        DocumentAction(this, new DocumentActionEventArgs(data["action"].Value<string>()));
                    break;
            }
        }

        void Socket_NewSessionConnected(WebSocketSession session)
        {
            SocketSession = session;
			if (WebSocketConnected != null)
				WebSocketConnected(this, null);
        }

        public void SetText(string text)
        {
            var message = new JObject();
            message.Add("command", "setText");
            message.Add("text", text);
            SocketSession.Send(message.ToString());
        }

        public void GetText(Action<string> callback)
        {
            PendingCallbacks.Add(Transaction, callback);
            var message = new JObject();
            message.Add("command", "getText");
            message.Add("transaction", Transaction++);
            SocketSession.Send(message.ToString());
        }

        public void SetKeyMap(string map)
        {
            var message = new JObject();
            message.Add("command", "setMode");
            message.Add("map", map);
            SocketSession.Send(message.ToString());
        }

        public IResponse RequestHandler(IRequest request)
        {
			var resource = "mdedit.Content" + request.Uri.LocalPath.Replace("/", ".");
            #if DEBUG
			Console.WriteLine("GET " + resource);
            #endif
			try
			{
	            var response = request.CreateResponse(HttpStatusCode.OK, "OK");
	            switch (Path.GetExtension(request.Uri.LocalPath))
	            {
	                case ".html":
	                    response.ContentType = "text/html";
	                    break;
	                case ".js":
	                    response.ContentType = "text/javascript";
	                    break;
	                case ".css":
	                    response.ContentType = "text/css";
	                    break;
	            }
	            response.Body = Assembly.GetExecutingAssembly().GetManifestResourceStream(resource);
	            return response;
			}
			catch
			{
				var response = request.CreateResponse(HttpStatusCode.InternalServerError, "Internal Server Error");
				return response;
			}
        }
    }
}