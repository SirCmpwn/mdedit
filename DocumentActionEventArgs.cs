﻿using System;

namespace mdedit
{
    public class DocumentActionEventArgs : EventArgs
    {
        public string Action { get; set; }

        public DocumentActionEventArgs(string action)
        {
            Action = action;
        }
    }
}